
$(document).ready(function () {
    function toast({title, type, message}){
    const main = document.getElementsByClassName("group-toast")
    const icons = {
        success :"fas fa-check",
        danger :"fas fa-exclamation-triangle",
        info :"fas fa-info",
        warning :"fas fa-exclamation",
    }
        if(main){
            const toast = document.createElement('div')
            toast.classList.add("toast",`toast--${type}`);
            toast.innerHTML=
            `<div class="toast--header">
                <div class="toast--icon"><i class="${icons[type]}"></i></div>
                <div class="toast--title">${title}</div>
                <div class="toast--close"><i class="fas fa-times"></i></div>
            </div>
            <div class="toast--message">${message}</div>`
            $(".group-toast").append(toast)
            setTimeout(function(){
                $(toast).remove();
            },4000) 

            $(toast).on("click",".toast--close",() =>{
                $(toast).remove();
            })
        }
}
    $(".btn--success").click(function(){
        toast({title : "Success", type : "success",message : "Message Success !!!!"})
    })
    $(".btn--info").click(function(){
        toast({title : "Info", type : "info",message : "Message Info !!!!"})
    })
    $(".btn--danger").click(function(){
        toast({title : "Danger", type : "danger",message : "Message Danger !!!!"})
    })
    $(".btn--warning").click(function(){
        toast({title : "Warning", type : "warning",message : "Message Warning !!!!"})
    })
});


